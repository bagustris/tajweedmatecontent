﻿الله kelimesinde lam'ın iki hali vardır:
• İnce okunma durumu. "Allah" kelimesindeki elif veya kelimenin önündeki harfin esresi varsa, "Allah" kelimesindeki lâm ince olarak okunur.
• Kalın okunma durumu. "Allah" kelimesindeki elif veya kelimenin önündeki harfte bir üstün veya ötre varsa, "Allah" kelimesindeki lâm kalın okunur.