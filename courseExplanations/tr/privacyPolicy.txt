﻿TajweedMate Uygulaması, Kur'an-ı Kerim öğrenme sürecinde arkadaşınız olmak için geliştirilmiştir. Uygulama, disiplinler arası profesyonellerden oluşan bir ekip tarafından en yeni teknolojileri ve en iyi yöntemleri uygulayarak şevk, sabır ve sıkı çalışmayla geliştirilmiştir. Ancak TajweedMate hala yoğun bir geliştirme sürecindedir. Profesyonel bir öğretmenin yerini alması da amaçlanmamıştır. Yani .. en azından, mevcut teknolojiler ve yakın gelecek için.

TajweedMate, en gelişmiş Yapay Zeka teknolojilerini kullanır. Konuşma Tanıma Motorunun eğitim sürecinde 40'tan fazla Kaarii'nin okuma kayıtları kullanılmaktadır. Bu motorun daha da mükemmelleşmesi, daha fazla kişinin okuma seslerinden öğrenmeyi gerektirir. Daha profesyonel okuyucular bulmak için elimizden gelenin en iyisini yapıyoruz, ancak sesli yanıtlarınızı sınavlara ve ezberleme bölümlerine göndererek motora katkıda bulunabilirsiniz. Gönderme varsayılan olarak devre dışıdır, ancak uygulama ayarlarından etkinleştirmeyi seçebilirsiniz. Kullanıcımızın gizliliğine ihtiyatla önem veriyoruz. Aşağıdaki bölümler gizlilik politikamızı yansıtmaktadır. Okunabilirlik adına Soru-Cevap stilini tercih ettik.


> Kullanıcılarımızdan ne tür bilgiler topluyoruz?
TajweedMate, "kaydet" ile başlatılan ve "durdur" düğmeleriyle tamamlanan sınav ve ezberleme yanıt kayıtlarını kaydeder. Bu ses kayıtları yalnızca Konuşma Tanıma Motoru tarafından ÇEVRİMDIŞI değerlendirme için kullanılır, bu nedenle herhangi bir yere veya herhangi bir kişiye gönderilmez. Kullanıcı kasıtlı olarak uygulama ayarlarından gönderme seçeneğini etkinleştirirse, bu ses kayıtlarını her seferinde tek bir yanıt için kasıtlı olarak "gönder düğmesini" tıklayarak gönderebilir. Gönderme işlemi sırasında, kullanıcının anonimliğini korumak için rastgele oluşturulmuş bir kullanıcı numarasıyla yalnızca bir ses kaydı gönderilir. Bu kullanıcı numarası, tanımlanabilir kullanıcı kimliği değildir, sadece her kullanıcı için yeni bir giriş oluşturmak için kullanılır. Kullanıcı tarafından veri silme talebi olması durumunda da bu numara zorunludur. Gelecekte, varsa, kayıt sorunlarını çözmek için bu ses kayıtlarına bir işletim sistemi bilgisi ekleyebiliriz. Yine, herhangi bir bilgi göndermek tamamen isteğinize bağlıdır.


> Bu ses kayıtlarını neden topluyoruz?
Normalde, TajweedMate değerlendirmesini ÇEVRİMDIŞI olarak gerçekleştirir, yani herhangi bir yere herhangi bir veri göndermez. Ancak, Konuşma Tanıma Motorunun daha da geliştirilmesine katkıda bulunmak istiyorsanız, ses kayıtlarınızı göndermenizden memnun oluruz. Bu ses kayıtları Konuşma Tanıma Motorunu geliştirmek için kullanılacaktır. Veritabanında yeni bir giriş oluşturmak için kullanıcının kimliğinin açığa çıkmasına izin vermeyen rastgele oluşturulmuş kullanıcı numarası kullanılır. Kayıtlara eşlik eden bir işletim sistemi bilgisi (Android 10.0 veya IOS 11.0 gibi), yalnızca hata ayıklama ve istatistiksel amaçlar için kullanmak üzere gelecekte eklenebilir.


> Verileri nasıl göndereceğiz? Şifrelenmiş olarak mı?
Evet. Kullanıcı verileri, https protokolü olarak bilinen güvenli bir bağlantı üzerinden gönderilir.


> Verilerinize nasıl erişebilir veya verilerinizin silinmesini nasıl talep edebilirsiniz?
Ses kayıtlarınızı ilgili sayfalarda (kaydınızı yaptığınız yerde) dinleyebilirsiniz. Verileri kasıtlı olarak "gönderme özelliğini" etkinleştirip" ayrıca "gönder" düğmesini tıklarsanız, uygulama ses kaydınızı sunucumuza gönderir. Geliştiriciye "Veri silme talebi" cümlesiyle açıkça belirterek bir e-posta ile silme talebi gönderebilirsiniz. Ses kayıtlarınız tanımlanabilir herhangi bir veriye sahip olmadığından, rastgele oluşturulmuş kullanıcı kimliğinizi bu taleple beraber paylaşmanız gerekir. Kayıtlarınızı sileceğiz (aslında eşleşen kullanıcı kimliği kayıtları). Şu anda uygulama bir hesap giriş mekanizması kullanmamaktadır, bu nedenle önerilen kullanıcı kimliğiniz mevcut herhangi bir kullanıcı kimliğiyle eşleşirse kayıtları sileceğiz. İstekleriniz sürekli olarak mevcut olmayan bir kullanıcı kimliği içeriyorsa, e-posta adresiniz (bize talebinizi ilettiğiniz) gelecekteki talepler için kara listeye alınabilir.


> Verilerinizi satıyor muyuz?
Hayır, eğer gönderdiyseniz, verilerinizi Motoru iyileştirmek için kullanıyoruz. Ses kayıtlarınızı satmayacağız. Konuşma Tanıma Motoru ise ileride satılabilir. Ancak kişisel verilerinizi veya ses kayıtlarınızı Motorun kendisinden almak imkansızdır.


> Herhangi bir üçüncü taraf reklam aracı kullanıyor muyuz?
Hayır, herhangi bir üçüncü taraf reklam aracı kullanmıyoruz.


> Kullanıcı verilerini ne kadar süre saklayacağız?
Motor için kullandığımız sürece saklayabiliriz. Motorun farklı versiyonlarını geliştirebiliriz, böylece verileri kullanıcı silme talebinde bulunana kadar saklayabiliriz.


> Bir kullanıcı bu politika veya uygulamanın başka herhangi bir kısmı hakkında daha fazla açıklama isterse ne olur?
Kullanıcılar, e-postamız (tajweedmate@gmail.com) veya diğer sosyal medya hesapları aracılığıyla herhangi bir soru sorabilir. En iyi cevapları vermek için elimizden geleni yapacağız.


> Bu gizlilik politikasını değiştirirsek ne olur?
Kullanıcılar en güncel politika için her zaman tajweedmate.com/privacy.html adresini ziyaret edebilir. Gelecekte herhangi bir değişiklik hakkında sizi bilgilendirmek için bir bildirim mekanizması ekleyebiliriz.


Ayrıca bu politikanın orijinal metninin İngilizce olduğunu lütfen unutmayın. Orijinal ile çeviri arasında herhangi bir tutarsızlık olursa, İngilizce metin geçerlidir.
