Commençons par une session de questions-réponses pour intégrer certains termes et définitions fondamentaux concernant les règles liées au Tanwin et au Sâkin Noon ( ْن et ً ٍ ٌ ) dans la récitation coranique avant de passer à des concepts plus complexes.

Q : Qu'est-ce que le Tanwin ?

R : Le Tanwin signifie deux Fatha (Nasb), deux Kasra (Jar), et deux Dhamma (Raf) dans le Coran ( ً ٍ ٌ ).

Q : Qu'est-ce que le Sâkin Noon ?

R : Le Sâkin Noon désigne un Noon avec Sukoon (ou Jazm) dessus ( ْن ).

Q : Quelle est la question concernant le Tanwin et le Sâkin Noon ?

R : Dans le Coran, chaque fois que vous lisez et que vous voyez Tanwin et Sâkin Noon, il y aura TOUJOURS une règle de récitation liée à la prononciation de la lettre noon.

Q : Combien de règles existe-t-il et quelles sont-elles ?

R : Il existe quatre règles liées au Tanwin et au Sâkin Noon. À savoir, (1) Izhaar, (2) Idghaam , (3) Iqlaab , et (4) Ikhfa.

Q : Quels sont les critères que j'utiliserai pour identifier ces règles dans le Coran ?

R : Pour les règles, vous devez regarder la lettre après le Sâkin Noon ou le Tanwin pour déterminer quelle règle appliquer.

Q : Pourriez-vous élaborer sur ces règles et sur les lettres qui nous aideront à déterminer quelle règle appliquer ?

R : Bien sûr ! Permettez-moi de vous donner une formule pour chaque règle :

Izhaar : (ا,ح,خ,ع,غ,ه)+(نْ ou ـًـٍـٌ)
2.a. Idghaam avec Ghunnah : (ن,و,ي,م)+(نْ ou ـًـٍـٌ)

2.b. Idghaam sans Ghunnah : (ل,ر)+(نْ ou ـًـٍـٌ)

Iqlaab : (ب)+(نْ ou ـًـٍـٌ)

Ikhfaa : (ت,ث,ج,د,ذ,ز,س,ش,ص,ض,ط,ظ,ف,ق,ك)+(نْ ou ـًـٍـٌ)

Q : Vous avez dit qu'il n'y a que quatre règles, mais vous en avez montré cinq ! Alors, qu'est-ce que le Ghunnah ?

R : Normalement, le ghunna se produit chaque fois qu'un shadda ّ  apparaît sur le Noon ن ou le Meem م , le récitateur doit faire vibrer le son par le nez. Dans la règle de l'idghaam avec ghunna, nous faisons la même chose avec les 4 lettres ci-dessus liées à cette règle, car nous devons sauter le noon et relier la lettre avant le noon et la lettre après le noon avec shadda ; nous faisons la même chose pour l'idghaam sans ghunna, mais nous ne faisons pas vibrer le son par le nez, ce qui est la différence entre l'idghaam avec ghunna et l'idghaam sans ghunna. Vous comprendrez mieux avec des exemples.

Q : Comment puis-je mémoriser toutes ces lettres ? N'est-ce pas trop difficile ?

R : Non, ce n'est pas le cas ! Laissez-moi vous donner un conseil. Les lettres de l'Izhaar sont des lettres de la gorge. La lettre de l'Iqlaab est seulement le ب, il n'y a que 6 lettres d'idghaam, et le reste seront des lettres d'ikhfaa.

Q : Y a-t-il un autre conseil que vous souhaitez me donner pour faciliter ma tâche ?

R : Oui, bien sûr ! Si vous voyez Tanwin et Sâkin Noon ( ْن et ً ٍ ٌ ) n'importe où dans le Coran, il doit y avoir 4 règles liées à la prononciation du noon. Vous (1) prononcerez (ou montrerez) le noon (Izhaar), (2) vous cacherez le noon (ikhfaa), (3) vous sauterez le noon et relierez les lettres avant et après le noon (idghaam), et enfin (4) vous transformerez le son du noon en son de meem (Iqlaab). C'est tout !

Q : Y a-t-il des exceptions concernant le Tanwin et le Sâkin Noon ?

R : Oui, il y a des mots dans le Coran qui répondent aux exigences du Tanwin et du Sâkin Noon ( ْن et ً ٍ ٌ ), mais nous ne faisons rien, nous les lisons simplement telles quelles.

Q : Commençons alors la pratique !

R : D'accord. Allons-y ! Vous pouvez maintenant passer à la leçon suivante. Bonne chance !