Fathah adalah garis miring yang ditulis di atas huruf (seperti dalam contoh).
Fa-ta-ha berarti membuka sesuatu. Jadi kita membuka mulut saat kita membacanya.Untuk huruf-huruf non-menarik kami mengucapkan frontal 'i' (seperti di tempat tidur), dan untuk huruf tegas kami mengucapkan 'a' (seperti di ayam).

Misalnya, huruf "Jim" dibacakan "Ja" ketika ada fathah di atasnya.
Huruf "RA" (ر) selalu tebal dengan fathah.

Saat menyelesaikan latihan, jaga poin-poin ini di pikiran Anda:
• Saat membaca suku kata dengan suku kata, penting untuk tidak memanjang suara.
• Seperti yang dapat Anda lihat dari contoh-contoh di bawah ini, ketika huruf ditulis bersama untuk membentuk kata-kata, mereka kehilangan "ekor" yang lebih rendah. Untuk membiasakan diri dengan ini, seseorang harus membiasakan diri dengan sejumlah besar contoh.
• Huruf mengalami perubahan saat menghubungkannya secara tertulis. Beberapa dapat terhubung dengan huruf yang datang sebelum dan sesudah sementara beberapa hanya terhubung dengan yang ada di hadapan mereka.
• Huruf “د ذ ر ز ا و” tidak terhubung ke yang setelahnya.
• Jika Anda memperhatikan huruf -huruf berwarna merah, Anda dapat melihat perubahan yang lebih baik saat menghubungkan huruf.
Mari kita mulai 😊: