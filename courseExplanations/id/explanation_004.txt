Dalam bahasa Arab, waw seperti ikal kecil yang ditempatkan di atas sebuah huruf disebut dhommah (ـــُــ). Ketika Dhommah berada di atas huruf kasar, itu diucapkan dengan suara U. Dengan huruf lembut, dhommah memiliki pengucapan yang lebih lembut.

Saat menyelesaikan latihan, tolong jaga poin-poin ini di pikiran Anda:
• Saat membaca suku kata dengan suku kata, penting untuk tidak memanjang suara.
• Seperti yang dapat Anda lihat dari contoh-contoh di bawah ini, ketika huruf ditulis bersama untuk membentuk kata -kata, mereka kehilangan "ekor" yang lebih rendah.Untuk membiasakan diri dengan ini, seseorang harus mengerjakan sejumlah besar contoh.
• huruf mengalami perubahan saat menghubungkannya secara tertulis. Beberapa dapat terhubung dengan huruf yang datang sebelum dan sesudah sementara beberapa hanya dapat terhubung dengan huruf setelah mereka.
• Huruf “د ذ ر ز ا و” tidak terhubung ke yang setelahnya.
• Jika Anda memperhatikan huruf-huruf berwarna merah, Anda dapat melihat perubahan yang lebih baik saat menghubungkan huruf.
Mari kita mulai 😊: