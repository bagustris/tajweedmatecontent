یہ اس وقت ہوتا ہے جب دو ایک جیسے حروف مل جاتے ہیں۔ ادغام کا مطلب ہے جوڑنا، یا اکٹھا کرنا۔ اگر ایک ہی حرف ایک ہی لفظ میں یا دو مختلف الفاظ میں ظاہر ہوتا ہے جو ایک دوسرے کے پیچھے آتے ہیں - پہلے میں ’سکون‘ اور دوسرے میں چھوٹا حرف ہو تو یہ ادغام متلائن بن جاتا ہے۔ چنانچہ پہلا حرف دوسرے میں ملایا جاتا ہے، اس لیے دوسرا حرف شدہ ہوتا ہے۔
دو حروف کو شدہ کے ساتھ ایک کے طور پر تلفظ کیا جاتا ہے۔
آئیے مشق شروع کریں 😊:
