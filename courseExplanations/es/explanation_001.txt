﻿• Hay 28 letras en el Sagrado Corán.
• Para recitar el Corán correctamente, es importante pronunciar las letras según las reglas de pronunciación, prestando atención a la calidad de las vocales.
• La pronunciación correcta de las letras debe aprenderse de un maestro recitador.
• La “لا” no es una letra separada sino una combinación de Lam y Alif.
• De estas letras, 7 son de sonido grueso (consonantes enfáticas) escritas en rojo: (خ ص ض ط ظ غ ق) Sin embargo, estas letras son letras de isti'laa. Eso significa que estas letras deben pronunciarse con la boca llena. Todas las demás letras deben pronunciarse con media boca, excepto la letra de (ر) raa que tiene condiciones únicas en su pronunciación.
• 3 son letras que suenan ceceando (ث ذ ظ)
Empecemos 😊: