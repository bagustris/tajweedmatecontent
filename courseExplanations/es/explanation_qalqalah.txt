Qalqalah significa literalmente estar en movimiento. Es el temblor de la voz. Hay 5 letras qalqalah en total:
ق ط ب ج د
Es necesario hacer Qalqalah en esas letras cuando tienen sukoon debido a sus cualidades [Sifaat: jahrr y shiddah (detención del brath’ y del flujo del sonido)] estas letras no emitirán ningún sonido.
Simplemente imita los ejemplos a continuación y concéntrate en las manchas de color rojo mientras lo haces.