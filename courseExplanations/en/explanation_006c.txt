• Madd means prolonging.
• When a letter with a short vowel is followed by a letter that has no short vowel, the vowel of the previous letter is elongated as long as the measure of one Alif (about one second).
• This letter without a vowel is called madd letter (ا و ي), meaning long vowel.
• If the long vowel (و) comes after a letter, its sound is lengthened to twice the length of a short vowel and is recited with an ‘oo’ sound.
• The letter before the long vowel (و) always has a damma.
• In some examples, the letter alif after the long vowel (و) signifies plurality and does not affect recitation in any way.
Let’s start 😊: